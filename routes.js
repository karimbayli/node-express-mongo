const express = require('express')
const Joi = require('@hapi/joi')
const {collection} = require('./db')

const router = express.Router()

const itemSchema = Joi.object().keys({
  name: Joi.string(),
  quantity: Joi.number().integer().min(0)
})

router.get('/api/earthquakes', (req, res) => {
  console.log(collection('earthquake'))
  var arr = collection('earthquake').find({}).toArray()
  arr.then(data=>{res.status(200).json(data)})
  
})

router.get('/api/droughts', (req, res) => {
  console.log(collection('drought'))
  var arr = collection('drought').find({}).toArray()
  arr.then(data=>{res.status(200).json(data)})
  
})

router.get('/api/earthquake_predictions', (req, res) => {
  console.log(collection('earthquake_prediction'))
  var arr = collection('earthquake_prediction').find({}).toArray()
  arr.then(data=>{res.status(200).json(data)})
  
})

module.exports = router
