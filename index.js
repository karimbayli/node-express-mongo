const express = require('express')
const bodyParser = require('body-parser')
const { init } = require('./db')
const routes = require('./routes')
const cors = require('cors')

var options = {
  origin: 'http://localhost:5555',
  optionsSuccessStatus: 200
}

const app = express()
app.use(bodyParser.json())
app.use(cors(options))
app.use(routes)


init().then(() => {
  
  app.listen(3000,function(){
    console.log('starting server on port 3000')
  })
})
