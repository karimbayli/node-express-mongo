const { MongoClient, ObjectId } = require('mongodb')

const connectionUrl = 'mongodb://XXXXXX:port'
const dbName = 'dbName'

let db

const init = () =>
  MongoClient.connect(connectionUrl, { useNewUrlParser: true }).then((client) => {
    db = client.db(dbName)
  })

const collection = (name) => {
  return db.collection(name)
}

module.exports = {collection, init}